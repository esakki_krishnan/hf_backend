const { get } = require('axios');
const { join } = require('path');
const { toJson } = require('parse-kml');

let MapData = null;
const LOCATION_URL='https://geocode.search.hereapi.com/v1/geocode';
const API_KEY='A83lSvYQXxwTFGVCkz0vhi9CZTWma72V-dgCMbNn0JY';

function parseKML(path) {
    return toJson(join(__dirname, '../', 'kml', 'FullStackTest_DeliveryAreas.kml'));
}

function checkPoly(point, polygon) {
    if(polygon[0] != polygon[polygon.length-1])
        polygon[polygon.length] = polygon[0];
    let j = 0;
    let oddNodes = false;
    let x = point[1];
    let y = point[0];
    let n = polygon.length;
    for (let i = 0; i < n; i++)
    {
        j++;
        if (j == n)
        {
            j = 0;
        }
        if (((polygon[i][0] < y) && (polygon[j][0] >= y)) || ((polygon[j][0] < y) && (polygon[i][0] >=
            y)))
        {
            if (polygon[i][1] + (y - polygon[i][0]) / (polygon[j][0] - polygon[i][0]) * (polygon[j][1] -
                polygon[i][1]) < x)
            {
                oddNodes = !oddNodes;
            }
        }
    }
    return oddNodes;
}

function checkPoint(point, checkPoint) {
    if(point[0] === checkPoint[0] && point[1] === checkPoint[1]) { return true }
    return false;
}

function getLocationDetail(location) {
    return get(LOCATION_URL + "?q="+encodeURIComponent(location)+"&apiKey="+API_KEY);
}

async function updateMapData() {
    try {
        const filePath = join(__dirname, '../', 'kml', 'FullStackTest_DeliveryAreas.kml');
        const data = await parseKML(filePath);
        MapData = data;
        return true;
    } catch(e) {
        console.error(e);
        throw new Error('Unable to update map data');
    }
}

function getMapData() {
    if(MapData)
        return MapData;
    throw new Error('MapData not available please update');
}

function conatinsLocation(locPoint) {
    let containedLoc = null;
    for (let i = 0; i < MapData.features.length; i++) {
        const currentLoc = MapData.features[i];
        const cords = currentLoc.geometry.coordinates;
        let isIn = false;
        if(currentLoc.geometry.type === 'Point') {
            isIn = checkPoint(locPoint, [cords[0],cords[1]]);
        } else {
            // console.log('cords.map(e => ([e[0], e[1]]))', );
            isIn = checkPoly(locPoint, cords[0].map(e => [e[0], e[1]]));
        }
        if(isIn){
            containedLoc = currentLoc;
            break;
        }
    }
    return containedLoc;
}

module.exports = {
    conatinsLocation,
    updateMapData,
    getMapData,
    getLocationDetail
}