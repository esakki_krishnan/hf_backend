
const { createServer, plugins} = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const { updateMapData } = require('./mapService');
const { getLocation } = require('./hotelController');

function setupServer() {
    const server = createServer();
    const cors = corsMiddleware({
        origins: ['*'],
        allowHeaders: ['*'],
        exposeHeaders: ['*']
    });
    server.pre(cors.preflight)
    server.use(cors.actual)
    server.use(plugins.queryParser());
    return server;
}

function setupRoutes(server) {
    server.get('/hotels', getLocation);
}

async function run() {
    try {
        // update Data
        await updateMapData();
        const server = setupServer();
        setupRoutes(server);
        server.listen(8000, function() {
            console.log('%s listening at %s', server.name, server.url);
        })
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
}

run();


