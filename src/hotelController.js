const { getLocationDetail, conatinsLocation } = require('./mapService');
async function getLocation(req, res) {
    try {
        const query = req.query.location;
        if(!query) {
            new Error({ code: 400, message: 'location parameter not provide' });
        }
        const data = await getLocationDetail(req.query.location);
        if(data.data.items.length === 0) { throw new Error({ code: 404, message: 'Location not found' }) }
        const loc = data.data.items[0];
        const thisLoc = [loc.position.lng, loc.position.lat];
        const nearHotel = conatinsLocation(thisLoc)
        res.send({  
            query: req.query, 
            location: loc, 
            hotel: { status: nearHotel ? 'OK': 'NOT FOUND', data: nearHotel ? nearHotel : undefined, error: nearHotel ? undefined : 'NOT FOUND' }
        });
    } catch(e) {
        console.log('error', e);
        res.send(e.code || 404, { error: e.message || 'Location not found' });
    }
}


module.exports = {
    getLocation
}