// const { toGeoJson } = require('gtran-kml');
const { join } = require('path');
const { toJson } = require('parse-kml');
// const { promisify } = require('util');
// const fs = require('fs')

// const APP_ID = 'W3edvOZ6oYBYzj0MoJp0';
// const API_KEY = 'A83lSvYQXxwTFGVCkz0vhi9CZTWma72V-dgCMbNn0JY'


// const storeData = (data, path) => {
//   try {
//     fs.writeFileSync(path, JSON.stringify(data))
//   } catch (err) {
//     console.error(err)
//   }
// }
// toGeoJson(join(__dirname, '../', 'kml', 'FullStackTest_DeliveryAreas.kml'))
// .then(d => {
//     console.log('data', d);
//     storeData(d, join(__dirname, '../', 'kml', 'test.json'));
// })
// .catch(e => {
//     console.log('e', e);
// })

// toJson(join(__dirname, '../', 'kml', 'FullStackTest_DeliveryAreas.kml'))
// .then(d => {
//     console.log('data', d);
//     storeData(d, join(__dirname, '../', 'kml', 'parse_kml.json'));
// })
// .catch(e => {
//     console.log('e', e);
// })
let mapData = null;
toJson(join(__dirname, '../', 'kml', 'FullStackTest_DeliveryAreas.kml'))
.then(d => {
    mapData = d;
})
.catch(e => {
    console.log('e', e);
})

const { createServer, plugins } = require('restify');
const axios = require('axios');
// const {createClient} = require('@google/maps');
const LOCATION_URL='https://geocode.search.hereapi.com/v1/geocode';
const API_KEY='A83lSvYQXxwTFGVCkz0vhi9CZTWma72V-dgCMbNn0JY';
// const GOOGLE_API_KEY = 'AIzaSyA3dIjTYzQyVunVF4GkNXL2TMiZCvKSUEU';
// const googleCLient = createClient({
//     key: GOOGLE_API_KEY
// });
function getLocationDetail(location) {
    return axios.get(LOCATION_URL + "?q="+encodeURIComponent(location)+"&apiKey=A83lSvYQXxwTFGVCkz0vhi9CZTWma72V-dgCMbNn0JY");
}

// function googleLoc(location) {
//     return new Promise(function(resolve, reject) {
//         googleCLient.geocode({address: location}, function(err, data) {
//             if(err) { reject(err) }
//             else { resolve(data.data) }
//         })
//     })
//     // return promGeoCode({ address: location });
// }

function checkPoly(point, polygon) {
    if(polygon[0] != polygon[polygon.length-1])
    polygon[polygon.length] = polygon[0];
    let j = 0;
    let oddNodes = false;
    let x = point[1];
    let y = point[0];
    let n = polygon.length;
for (let i = 0; i < n; i++)
{
    j++;
    if (j == n)
    {
        j = 0;
    }
    if (((polygon[i][0] < y) && (polygon[j][0] >= y)) || ((polygon[j][0] < y) && (polygon[i][0] >=
        y)))
    {
        if (polygon[i][1] + (y - polygon[i][0]) / (polygon[j][0] - polygon[i][0]) * (polygon[j][1] -
            polygon[i][1]) < x)
        {
            oddNodes = !oddNodes;
        }
    }
}
return oddNodes;
}

function checkPoint(point, checkPoint) {
    if(point[0] === checkPoint[0] && point[1] === checkPoint[1]) { return true }
    return false;
}

function conatinsLocation(locPoint) {
    console.log(locPoint);
    let containedLoc = null;
    for (let i = 0; i < mapData.features.length; i++) {
        const currentLoc = mapData.features[i];
        const cords = currentLoc.geometry.coordinates;
        let isIn = false;
        if(currentLoc.geometry.type === 'Point') {
            isIn = checkPoint(locPoint, [cords[0],cords[1]]);
        } else {
            // console.log('cords.map(e => ([e[0], e[1]]))', );
            isIn = checkPoly(locPoint, cords[0].map(e => [e[0], e[1]]));
        }
        if(isIn){
            containedLoc = currentLoc;
            break;
        }
    }
    return containedLoc;
}
const server = createServer();
server.use(plugins.queryParser());
server.get('/hotels', async function(req, res) {
    try {
        const data = await getLocationDetail(req.query.location);
        // const googleD = await googleLoc(req.query.location);
        // console.log('ggole', googleD.data);
        // "position": {
        //     "lat": 48.19495,
        //     "lng": 16.34314
        // },
        // 16.370487,
        //         48.1958732,
        const position = data.data.items[0].position;
        const thisLoc = [position.lng, position.lat];
        console.log('pos', conatinsLocation(thisLoc));
        console.log(mapData.features.length);
        res.send({ hello: 'world', query: req.query, data: data.data });
    } catch(e) {
        console.log('error', e);
        res.send(404, { error: 'Location not found' });
    }
})
server.listen(8000, function() {
    console.log('%s listening at %s', server.name, server.url);
})

